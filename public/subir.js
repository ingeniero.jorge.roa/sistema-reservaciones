$(document).ready(function(){

    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $("#top").slideDown(1000);
        } else {
            $("#top").slideUp();
        }
      
      });
      
      $("#top").click(function(){
          $("html , body").animate({ scrollTop: 0 }, 1000);
          return false;
      });     

});
