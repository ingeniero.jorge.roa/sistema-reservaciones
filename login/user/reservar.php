<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../public/style.css">
    <link rel="stylesheet" href="../../public/boostrap.min.css">
    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
    <script src="../../public/date.js"></script>
    <title>FORMULARIO DE RESERVAR</title>
</head>
<body class="animate__animated animate__bounce body" style="background-color:#F0FFFF;">
    <br>
    <br>
    <br>
    <br>
<?php
$conn = new mysqli("localhost", "root", "", "sistemareservaciones");

$id = $_GET['id'];
$sql="SELECT * FROM mesas WHERE id LIKE '%$id%'";
    

$res=$conn->query($sql);

while($row=$res->fetch_assoc()){
    
    
    ?>
    <br>
    <div class="container border border-dark border-3" id="ancho_ajustar" style=" border-radius: 20px;
    box-shadow: 15px 15px 15px black;">
        <div class="row">
            <div class="col-md-12 m-1" id="ancho_ajustar">

                    <form action="cofirmareserva.php" method="POST" class="m-1"> 
                        <div class="mb-3 text-center">
                            <img src="../../public/img/logo.png" alt="">
                        </div>
                        <div class="mb-3">
                            <input type="text" id="Nombre" name="Nombre" value="" class="form-control" placeholder="Nombre Completo"  required>
                        </div>
                        <div class="mb-3">
                             <input type="email" id="Correo" name="Correo" value="" class="form-control" placeholder="Correo Electrónico"   required>
                        </div>
                        <div class="mb-3">
                            <input type="text" id="Telefono" name="Telefono" value="" class="form-control" placeholder="Teléfono"  required>
                        </div>
                        <div class="mb-3">
                            <input type="text" id="Direcion" name="Direcion" value="" class="form-control" placeholder="Dirección" required >
                        </div>
                        <div class="mb-3">
                            <input type="date" id="fechaReserva" name="Fecha" min="2017-01-01" class="form-control" required >
                        </div>
                        <div class="mb-3">
                            <input type="hidden" id="id"  value="<?php echo $row['id'];?>" name ="id">
                        </div>
                        <div class="mb-3">
                        <div class="col">
                         <h6 class="text-center alert alert-danger" style="font-size:25px;">Para "RESERVAR" solo da clic en la opción de libre y selecciona "RESERVAR"</h6>
                        </div>
                        </div>
                        <div class="mb-3">
                        <table class="table m-1 text-center">
                        <thead>
                            <tr>
                            <th scope="col"><strong>MESA ELEGIDA</strong>  </th>
                            <th scope="col"><strong>DESCRIPCIÓN DE MESA</strong>  </th>
                            <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                                <th style=" background-color:#F0FFFF;"><?php echo $row['NumeroMesa']?></th>
                                <td style=" background-color:#F0FFFF;"><?php echo $row['Descripcion']?></td>
                                <td> <select name ="estado" id="Estado" class="form-select" required>
                            <option value="<?php echo $row['Estado'];?>"><?php  echo $row['Estado'];?></option>
                            <option value="Reservar">reservar</option>
                        </select></td>
                              
            
                        </tr>
                     
                             
                        </tbody>
                
                </table>
                        </div>

                       
                    <div class="mb-3 text-center m-2">
                         <input type="submit" name="actualizar" class="btn btn-success text-center" value="RESERVAR">
                    </div>
                    
                    </form>
                    <?php
                        }  
                    ?>   
                </div>
            </div>
        </div>
    </div>
    
          
        <script>
            var anio = fecha.getFullYear();
            var dia = fecha.getDate();
            var _mes = fecha.getMonth();//viene con valores de 0 al 11
            _mes = _mes + 1;//ahora lo tienes de 1 al 12
            if (_mes < 10)//ahora le agregas un 0 para el formato date
            { var mes = "0" + _mes;}
            else
            { var mes = _mes.toString;}
            document.getElementById("fechaReserva").min = anio+'-'+mes+'-'+dia; 
        </script>
</body>
</html>
