<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../public/style.css">
    <link rel="stylesheet" href="../../public/boostrap.min.css">
    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
    <title>Document</title>
</head>
<body class="animate__animated animate__bounce body" style=" background-color:#F0FFFF;" >
    <br>
    <br>
    <br>
    <br>
    <br>

        <div class="container">
            <div class="row">
                <div class="col-12">
                     <h2 class="text-dark text-center"><strong>ELIGE TU MESA</strong> </h2>
                </div>
            </div>
        </div>
        <br>
        <div class="container text-center border border-dark border-3" style=" border-radius: 20px;
    box-shadow: 15px 15px 15px black;">
            <div class="col-md-12" style="">
                <table class="table m-1">
                        <thead>
                            <tr>
                            <th scope="col"><strong>NÚMEROS DE MESAS DISPONIBLES</strong>  </th>
                            <th scope="col"><strong>DESCRIPCIÓN DE MESA</strong>  </th>
                            <th scope="col"><strong>RESERVAR</strong>  </th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                    $conn = new mysqli("localhost", "root", "", "sistemareservaciones");
                    
                    $sql="SELECT * FROM mesas WHERE Estado LIKE '%libre%'";
                        
                    $res=$conn->query($sql);
                    
                    while($row=$res->fetch_assoc()){?>
                        <tr>
                                <th style=" background-color:#F0FFFF;"><?php echo $row['NumeroMesa']?></th>
                                <td style=" background-color:#F0FFFF;"><?php echo $row['Descripcion']?></td>
                                <td style=" background-color:#F0FFFF;"> <a  class="btn btn-info" href="reservar.php?id=<?php echo $row['id'];?>"> CLIC PARA RESERVAR</a></td>
            
                        </tr>
                        <?php
                    }
                    ?>
                             
                        </tbody>
                </table>
            
                  
                    
            </div>
        </div>


    <script src="../../public/boostrap.min.js"></script>
</body>
</html>