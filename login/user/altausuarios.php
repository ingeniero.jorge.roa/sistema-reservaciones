

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../public/style.css">
    <link rel="stylesheet" href="../../public/boostrap.min.css">
    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
    <title>ALTA USURIO</title>
</head>
<body class="animate__animated animate__bounce body">

<br><br>
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="text-center"><strong>LLENA LOS CAMPOS REQUERIDOS</strong> </h2>
                    <h6 class="text-center alert alert-danger" style="font-size:25px;">Es necesario que te registres para poder llevar a cabo tu reservación</h6>
                </div>
            </div>
        </div>
    <br><br>
    <div class="container border border-dark border-3" id="ancho_ajustar" style=" border-radius: 20px;
    box-shadow: 15px 15px 15px black;">
        <div class="row">
            <div class="col-md-12"  id="ancho_ajustar">
                <br>

                <form action="validaraltauser.php" method="POST">
                    <div class="mb-3">

                    </div>
                    <div class="mb-3">
                      <input type="text" id="Nombre"   class="form-control"  name="Nombre"  placeholder="Nombre" value="" required>
                    </div>
                    <div class="mb-3">
                      <input type="email" id="Correo"  class="form-control"  name="Correo" placeholder="Correo electrónico" value="" required>
                     </div>
                     <div class="mb-3">
                      <input type="number" id="Telefono"   class="form-control" name="Telefono" placeholder="Teléfono" value="" required>
                    </div>
                    <div class="mb-3">
                      <input type="text" id="Ciudad"   class="form-control" name="Ciudad" placeholder="Ciudad" value="">
                    </div>
                    <div class="mb-3">
                      <input type="password" id="Contrasena"   class="form-control" name="Contrasena" placeholder="Contraseña" value="" required>
                    </div>
                    <br>
                    <div class="mb-3 text-center">
                     <input type="submit" name="Guardar" class="btn btn-success" value="Guardar">
                    </div>
                    <div class="mb-3 text-center">
                      <span style="font-size:20px;"><a href="datos_tecliar.php">Da clic aquí si ya estas registrado</a></span>
                    </div>
                    
                    <br>
                </form>





            </div>
        </div>
    </div>



</body>
</html>




