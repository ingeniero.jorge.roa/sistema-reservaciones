
<?php
  session_start();
  error_reporting(0);

  $varsesion = $_SESSION['user'];

  if ($varsesion == null || $varsesion = "") {
   echo ' <script>alert("Usted no tiene autorización");
          location.href = "../login.html"; </script>';
    die();
  }




?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../public/boostrap.min.css">
    <link rel="stylesheet" href="../../public/style.css">
    <link rel="stylesheet" href="../../public/fontawesome/css/all.min.css">
    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
    <title>DASHBOARD</title>
</head>
<body class="animate__animated animate__bounce body">


<div class="container-fluid">
  <div class="row align-items-start">
    <div class="col-2 bg-white text-white border border-dark border-3">
                <img src="../../public/img/LOGO.PNG" alt="" class="m-1"><br>
                <h6 class="text-black m-1 text-uppercase"><strong>BIENVENIDO <?php  echo $_SESSION['user']; ?></strong> </h6>
                <br>
                <h6  class="text-dark"><strong>ADMIN RESTAURANTE</strong> </h6>
                <p class="text-dark">
                        <ul style="list-style: none;" >
                        <p><a style="text-decoration:none;" id="selector_hover_dash" href="dashuser.php"><i class="fa-solid fa-address-book"></i><span class="text-dark"><strong> RESERVACIONES REALIZADAS</strong></span</a></p> 
                        <p><a style="text-decoration:none;" id="selector_hover_dash" href="dashuserquejas.php"><i class="fa-solid fa-tablet"></i><span class="text-dark"><strong> QUEJAS Y SUGERENCIAS</strong></span></a></p> 
                        <p><a style="text-decoration:none;" id="selector_hover_dash" href="usuariosregistrados.php"><i class="fa-solid fa-users"></i><span class="text-dark"><strong> PEDIDOS</strong></span></a></p> 
                               <p><a style="text-decoration:none;" id="selector_hover_dash" href="../../cerrar_sesion_user.php"><i class="fa-solid fa-lock"></i><span class="text-dark"><strong> CERRAR SESIÓN</strong></span></a></p> 
                          </ul>
                      </p>
                      <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    </div>
    <div class="col-10">
    <div class="container">
            <div class="row">
                <div class="col-12">
                     <h2 class="text-dark text-center"><strong>ELIGE TU MESA</strong> </h2>
                </div>
            </div>
        </div>
        <br>
        <div class="container text-center border border-dark border-3" style=" border-radius: 20px;
    box-shadow: 15px 15px 15px black;">
            <div class="col-md-12" style="">
                <table class="table m-1">
                        <thead>
                            <tr>
                            <th scope="col"><strong>NÚMEROS DE MESAS DISPONIBLES</strong>  </th>
                            <th scope="col"><strong>DESCRIPCIÓN DE MESA</strong>  </th>
                            <th scope="col"><strong>RESERVAR</strong>  </th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                    $conn = new mysqli("localhost", "root", "", "sistemareservaciones");
                    
                    $sql="SELECT * FROM mesas WHERE Estado LIKE '%libre%'";
                        
                    $res=$conn->query($sql);
                    
                    while($row=$res->fetch_assoc()){?>
                        <tr>
                                <th style=" background-color:#F0FFFF;"><?php echo $row['NumeroMesa']?></th>
                                <td style=" background-color:#F0FFFF;"><?php echo $row['Descripcion']?></td>
                                <td style=" background-color:#F0FFFF;"> <a  class="btn btn-info" href="reservar.php?id=<?php echo $row['id'];?>"> CLIC PARA RESERVAR</a></td>
            
                        </tr>
                        <?php
                    }
                    ?>
                             
                        </tbody>
                </table>
            
                  
                    
            </div>
        </div>
    </div>
  </div>

    
                    <!--<h6 id="cursor" class="text-dark">ADMIN RESTAURANTE</h6>
                    <div>
                      <p class="text-dark">
                      <div>
                      <p class="text-dark">
                        <ul>
                            <a id="selector_hover_dash" href="dashboard.html"><li class="text-dark">RESERVAR</li></a>
                            <a id="selector_hover_dash" href="dashboard.html"><li class="text-dark">RESERVADOS</li></a>
                            <a  id="selector_hover_dash" href="cerrar_sesion.php"><li class="text-dark">CERRAR SESIÓN</li></a>
                          </ul>
                      </p>
                      <img src="" alt="">
                    </div>
                      </p>
                    </div>
                    <h6 id="cursor" class="text-dark">SECCIÓN PENDIENTE</h6>
                    <div>
                      <p class="text-dark">
                      Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
                      Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero
                      ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis
                      lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                      </p>
                      <ul>
                        <li>List item one</li>
                        <li>List item two</li>
                        <li>List item three</li>
                      </ul>
                    </div>-->
                    
                     
       
     

      <script src="../../public/boostrap.min.js"></script>
      <script src="../../public/date.js"></script>
      <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
      <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js" integrity="sha256-xH4q8N0pEzrZMaRmd7gQVcTZiFei+HfRTBPJ1OGXC0k=" crossorigin="anonymous"></script>
      <script>
        $( function() {
          $( "#accordion" ).accordion();
        } );
        </script>
</body>
</html>