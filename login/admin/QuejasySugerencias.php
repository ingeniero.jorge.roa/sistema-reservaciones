


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../public/style.css">
    <link rel="stylesheet" href="../../public/boostrap.min.css">
    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
    <title>Quejas y Sugerencias</title>
</head>
<body class="animate__animated animate__bounce body">
    <br><br>
        <div class="container">
            <div class="row">
                <div class="col">
                    <h6 class="text-center"><strong>LLENA LOS CAMPOS REQUERIDOS</strong> </h6>
                </div>
            </div>
        </div>
    <br><br>
    <div class="container border border-dark border-3" id="ancho_ajustar" style=" border-radius: 20px;
    box-shadow: 15px 15px 15px black;">
        <div class="row">
            <div class="col-md-12"  id="ancho_ajustar">
                <br>
                <form action="validadquejas.php" method="POST" >
                <div class="mb-3">
                     <input type="text" id="Nombre" name="Nombre" value=""  class="form-control" placeholder="Ingresa tu nombre" required>
                </div>
                <div class="mb-3">
                    <input type="email" id="Correo" name="Correo" value=""  class="form-control" placeholder="Ingresa tu correo electrónico" required>
                </div>
                <div class="mb-3">
                    <input type="text" id="Ciudad" name="Ciudad" value=""  class="form-control" placeholder="Ingresa tu ciudad" required>
                </div>
                <div class="mb-3">
                    <textarea class="form-control" name="comentarios" rows="10" cols="40" placeholder="Escribe aquí tus comentarios"></textarea>
                </div>
                <div class="mb-3 text-center">
                  <input type="submit" name="actualizar" class="btn btn-success m-2 text-center" value="enviar">
                </div>
            </form>


            </div>
        </div>
    </div>
</body>
</html>




