
<?php
  session_start();
  error_reporting(0);

  $varsesion = $_SESSION['user'];

  if ($varsesion == null || $varsesion = "") {
   echo ' <script>alert("Usted no tiene autorización");
          location.href = "../login.html"; </script>';
    die();
  }




?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../public/boostrap.min.css">
    <link rel="stylesheet" href="../../public/style.css">
    <link rel="stylesheet" href="../../public/fontawesome/css/all.min.css">
    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
    <title>DASHBOARD</title>
</head>
<body class="animate__animated animate__bounce body">


<div class="container-fluid">
  <div class="row align-items-start">
    <div class="col-2 bg-white text-white border border-dark border-3">
                <img src="../../public/img/LOGO.PNG" alt="" class="m-1"><br>
                <h6 class="text-black m-1 text-uppercase"><strong>BIENVENIDO <?php  echo $_SESSION['user']; ?></strong> </h6>
                <br>
                <h6  class="text-dark"><strong>ADMIN RESTAURANTE</strong> </h6>
                <p class="text-dark">
                        <ul style="list-style: none;" >
                        <p><a style="text-decoration:none;" id="selector_hover_dash" href="dashboard.php"><i class="fa-solid fa-address-book"></i><span class="text-dark"><strong> RESERVADOS</strong></span</a></p> 
                        <p><a style="text-decoration:none;" id="selector_hover_dash" href="Consultaliberacion.php"><i class="fa-solid fa-tablet"></i><span class="text-dark"><strong> LIBERAR MESAS</strong></span></a></p> 
                        <p><a style="text-decoration:none;" id="selector_hover_dash" href="usuariosregistrados.php"><i class="fa-solid fa-users"></i><span class="text-dark"><strong> USUARIOS REGISTRADOS</strong></span></a></p> 
                            <!-- Example single danger button -->
                            <ul class="navbar-nav">
                              <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-dark" href="#" id="selector_hover_dash" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fa-solid fa-bars"></i> <span><STRONG>CONTROL MENÚ</STRONG> </span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                                  <li><a class="dropdown-item" href="#">AGREGAR PLATILLO</a></li>
                                  <li><a class="dropdown-item" href="#">ELIMINAR PLATILLO</a></li>
                                  <li><a class="dropdown-item" href="#">AGREGAR BEBIDA</a></li>
                                  <li><a class="dropdown-item" href="#">ELIMINAR BEBIDA</a></li>
                                  <li><a class="dropdown-item" href="#">AGREGAR POSTRE</a></li>
                                  <li><a class="dropdown-item" href="#">ELIMINAR POSTRE</a></li>
                                </ul>
                               <p><a style="text-decoration:none;" id="selector_hover_dash" href="mostrarquejas.php"><i class="fa-solid fa-envelope-open-text"></i><span class="text-dark"><strong> QUEJAS Y SUGERENCIAS</strong></span></a></p> 
                               <p><a style="text-decoration:none;" id="selector_hover_dash" href="../../cerrar_sesion.php"><i class="fa-solid fa-lock"></i><span class="text-dark"><strong> CERRAR SESIÓN</strong></span></a></p> 
                          </ul>
                      </p>
                      <br><br><br><br><br><br><br><br><br>
    </div>
    <div class="col-10">
        <div>
          <h3 class="text-center text-dark">RESERVACIONES REALIZADAS</h3>
        </div>
        <table class="table" style=" border-radius: 20px;
    box-shadow: 15px 15px 15px black;">
            <thead>
            <tr>
                <th scope="col">NOMBRE</th>
                <th scope="col">CORREO ELECTRÓNICO</th>
                <th scope="col">CIUDAD</th>
                <th scope="col">COMENTARIOS</th>
              </tr>
            </thead>
            <tbody>
            <?php
              $conn = new mysqli("localhost", "root", "", "sistemareservaciones");
              $sql="SELECT * FROM quejas";  
              $res=$conn->query($sql);
              while($row=$res->fetch_assoc()){?>
                <tr>
                    <th><?php echo $row['Nombre']?></th>
                    <td><?php echo $row['Correo']?></td>
                    <td><?php echo $row['Ciudad']?></td>
                    <td><?php echo $row['Comentarios']?></td>
                  </tr>
            </tbody>
              <?php
                  }
              ?> 
        </table>
    </div>
  </div>

    
                    <!--<h6 id="cursor" class="text-dark">ADMIN RESTAURANTE</h6>
                    <div>
                      <p class="text-dark">
                      <div>
                      <p class="text-dark">
                        <ul>
                            <a id="selector_hover_dash" href="dashboard.html"><li class="text-dark">RESERVAR</li></a>
                            <a id="selector_hover_dash" href="dashboard.html"><li class="text-dark">RESERVADOS</li></a>
                            <a  id="selector_hover_dash" href="cerrar_sesion.php"><li class="text-dark">CERRAR SESIÓN</li></a>
                          </ul>
                      </p>
                      <img src="" alt="">
                    </div>
                      </p>
                    </div>
                    <h6 id="cursor" class="text-dark">SECCIÓN PENDIENTE</h6>
                    <div>
                      <p class="text-dark">
                      Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
                      Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero
                      ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis
                      lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                      </p>
                      <ul>
                        <li>List item one</li>
                        <li>List item two</li>
                        <li>List item three</li>
                      </ul>
                    </div>-->
                    
                     
       
     

      <script src="../../public/boostrap.min.js"></script>
      <script src="../../public/date.js"></script>
      <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
      <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js" integrity="sha256-xH4q8N0pEzrZMaRmd7gQVcTZiFei+HfRTBPJ1OGXC0k=" crossorigin="anonymous"></script>
      <script>
        $( function() {
          $( "#accordion" ).accordion();
        } );
        </script>
</body>
</html>